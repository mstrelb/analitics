package com.mstrelb.analytics;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import com.mstrelb.analytics.databinding.ActivityMainBinding;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;
import com.segment.analytics.Traits;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnTrack.setOnClickListener(v -> Analytics.with(getApplicationContext()).track(binding.editTrack.getText().toString()));
        binding.btnIdentify.setOnClickListener(v -> Analytics.with(this)
                .identify(binding.editIdentifyId.getText().toString(), new Traits().putName(binding.editIdentifyName.getText().toString()), null));
        binding.btnScreen.setOnClickListener(v -> Analytics.with(this)
                .screen(binding.editScreenId.getText().toString(), new Properties().putValue(binding.editScreenName.getText().toString(), binding.editScreenValue.getText().toString())));
        binding.btnGroup.setOnClickListener(v -> Analytics.with(this)
                .group(binding.editGroupId.getText().toString(), new Traits().putName(binding.editGroupName.getText().toString()), null));
    }
}