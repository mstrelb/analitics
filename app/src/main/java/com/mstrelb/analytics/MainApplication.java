package com.mstrelb.analytics;

import android.app.Application;

import com.segment.analytics.Analytics;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Analytics analytics = new Analytics.Builder(this, "ANY_KEY")
                .trackApplicationLifecycleEvents() // Enable this to record certain application events automatically!
                .recordScreenViews() // Enable this to record screen views automatically!
                .logLevel(Analytics.LogLevel.VERBOSE)
                .connectionFactory(new MyConnectionFactory())
                .build();

        // Set the initialized instance as a globally accessible instance.
        Analytics.setSingletonInstance(analytics);
    }
}