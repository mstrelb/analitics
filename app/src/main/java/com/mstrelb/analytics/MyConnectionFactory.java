package com.mstrelb.analytics;

import com.segment.analytics.ConnectionFactory;

import java.io.IOException;
import java.net.HttpURLConnection;

public class MyConnectionFactory extends ConnectionFactory {
    public HttpURLConnection projectSettings(String writeKey) throws IOException {
        return openConnection(BuildConfig.URL);
    }

    public HttpURLConnection upload(String apiHost) throws IOException {
        HttpURLConnection connection = openConnection(String.format("%s", apiHost));
        connection.setRequestProperty("Content-Encoding", "");
        connection.setDoOutput(true);
        connection.setChunkedStreamingMode(0);
        return connection;
    }
}
